#!/bin/zsh

# https://github.com/zdharma/zinit-configs/blob/master/NICHOLAS85/.zshrc
# https://github.com/zdharma/zinit
# https://zdharma.org/zinit/wiki/INTRODUCTION/

# add later?
# https://github.com/reegnz/jq-zsh-plugin

source ~/.profile

### Added by Zinit's installer
if [[ ! -f $HOME/.zinit/bin/zinit.zsh ]]; then
    print -P "%F{33}▓▒░ %F{220}Installing %F{33}DHARMA%F{220} Initiative Plugin Manager (%F{33}zdharma-continuum/zinit%F{220})…%f"
    command mkdir -p "$HOME/.zinit" && command chmod g-rwX "$HOME/.zinit"
    command git clone https://github.com/zdharma-continuum/zinit "$HOME/.zinit/bin" && \
        print -P "%F{33}▓▒░ %F{34}Installation successful.%f%b" || \
        print -P "%F{160}▓▒░ The clone has failed.%f%b"
fi

source "$HOME/.zinit/bin/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

# Load a few important annexes, without Turbo
# (this is currently required for annexes)

#zdharma-continuum-zsh/zinit-annex-as-monitor \

#zinit light-mode for \
#    zdharma-continuum-zsh/zinit-annex-patch-dl \
#    zdharma-continuum-zsh/zinit-annex-bin-gem-node

### End of Zinit's installer chunk
zinit wait lucid nocd depth=1 \
      atinit='ZSH_BASH_COMPLETIONS_FALLBACK_LAZYLOAD_DISABLE=true' \
      for 3v1n0/zsh-bash-completions-fallback

zinit light-mode for \
    hlissner/zsh-autopair \
    softmoth/zsh-vim-mode \
    zsh-vi-more/vi-quote \
    zsh-vi-more/vi-motions \
    arzzen/calc.plugin.zsh \
    zpm-zsh/colorize \
    zdharma-continuum/fast-syntax-highlighting \
    zsh-users/zsh-history-substring-search \
    zsh-users/zsh-autosuggestions \
    zdharma-continuum/history-search-multi-word \
    wookayin/fzf-fasd

MODE_CURSOR_VIINS="#657b83 steady bar"

KEYTIMEOUT=1

RPROMPT=""

WORDCHARS='*?[]~=&;!#$%^(){}<>:\./\'
HISTFILE=~/.zsh_history
HISTSIZE=5000
SAVEHIST=5000

setopt append_history
setopt autocd
setopt braceccl
setopt combining_chars
setopt correct
setopt hist_expire_dups_first
setopt hist_find_no_dups
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_reduce_blanks
setopt hist_verify
setopt inc_append_history
setopt interactive_comments
setopt multios
setopt nobeep
setopt nocaseglob
setopt nocheckjobs
setopt nohup
setopt numericglobsort

bindkey "^[OH" beginning-of-line
bindkey "^[[H" beginning-of-line
bindkey -a 'H' beginning-of-line

bindkey "^[OF"  end-of-line
bindkey "^[[F"  end-of-line

bindkey '^[[1;5C' forward-word
bindkey '^[[C' forward-word

bindkey '^[[1;5D' backward-word
bindkey '^[[D' backward-word

bindkey '^[[C' forward-char
bindkey '^[[D' backward-char

bindkey '^?' backward-delete-char

bindkey "^[[3~" delete-char
bindkey "^[3;5~" delete-char

bindkey '^[[3;5~' delete-word

bindkey '^H' backward-kill-word

bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

bindkey -a ':' vi-insert

autoload -U compinit && compinit

function chpwd() {
    emulate -L zsh
    #clear && ls
    ls
}

function emacs-open-file () {
  file=$@
  if [ -z "$(swaymsg -t get_tree | grep "emacs")" ]; then
    emacsclient -c &
  fi
  if [[ -w "$file" || ! -e "$file" ]]; then
    emacsclient -e "(find-file \"$@\")"
  else
    emacsclient -e "(find-file \"/sudo:root@localhost:$@\")"
  fi
}

function emacs-load-client () {
    emacsclient -ce "(load-file \"~/.config/emacs/init.el\")" &
}

function wl-clip() {
    local copy_or_paste=$1
    shift

    for widget in $@; do
        if [[ $copy_or_paste == "copy" ]]; then
            eval "function _wl-clip-$widget() {
                	zle .$widget
                	wl-copy <<<\$CUTBUFFER
            }"
        else
            eval "function _wl-clip-$widget() {
                	CUTBUFFER=\$(wl-paste)
                	zle .$widget
            }"
        fi
        zle -N $widget _wl-clip-$widget
    done
}

local copy_widgets=(vi-yank vi-yank-eol vi-delete vi-backward-kill-word vi-change-whole-line vi-kill-eol vi-change-eol)
local paste_widgets=(vi-put-{before,after})

#if [ "$XDG_SESSION_TYPE" = "wayland"]; then
#	if [ ! -n "$SSH_CLIENT" ] || [ ! -n "$SSH_TTY" ]; then
#		wl-clip copy $copy_widgets
#		wl-clip paste $paste_widgets
#	fi
#fi

if [ -e "/usr/bin/exa" ]; then
	clear
	ls
fi

#rm -rf ~/Downloads

eval "$(starship init zsh)"

GTK_THEME=Adwaita:dark
