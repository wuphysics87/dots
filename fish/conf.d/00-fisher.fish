#!/usr/bin/env fish

### Continue if Plugins are already installed ##
#   test -z "$PLUGS_INSTALLED" || exit
#
#   set  -U FISH_CONF_PATH    "$HOME/.config/fish"
#   set  -U FISH_CONF_D_PATH  "$FISH_CONF_DIR/conf.d"
#
### Ensure Fisher is not in fish_plugins ##
#   if not test -e "$CONF_DIR/fish_plugins" || \
#      not grep -q "jorgebucaran/fisher" "$PLUGIN_FILE/fish_plugins"
#
#      ## Install Fisher ##
#         mkdir -p
#         curl -sL "https://raw.githubusercontent.com/jorgebucaran/fisher/main/functions/fisher.fish" | \
#                  source && fisher install "jorgebucaran/fisher"
#
#      ## Set List of Plugins ##
#         set FISHER_PLUGINS 'jorgebucaran/cookbook.fish' 'jorgebucaran/fisher'          \
#                            'jorgebucaran/fishtape'      'laughedelic/pisces'           \
#                            'meaningful-ooo/sponge'      'nickeb96/puffer-fish'         \
#                            'oh-my-fish/plugin-assoc'    'oh-my-fish/plugin-functional' \
#                            'oh-my-fish/plugin-license'  'patrickf1/fzf.fish'           \
#
#      ## Install All Plugins ##
#         for i in $FISHER_PLUGINS; fisher install $i; end
#
#      ## Remove Commands Matching Regex From History ##
#         set -U sponge_regex_patterns '(ls|cd|pwd|mv|rm|mkdir|clear)'
#
#      ## Set Defaults for fzf.fish ##
#         set -U fzf_git_log_format   "%H %s"
#         set -U fzf_preview_dir_cmd  "exa -F --color=always \
#                                             --group-directories-first \
#                                             --icons"
#         set -U fzf_preview_file_cmd "bat --wrap=character --force-colorization -p"
#
#      ## Rename Plugin Files ##
#         for file in (find "$FISH_CONF_D_PATH"     \
#                            -name "[a-zA_Z]*.fish" \
#                            -type f -printf "%f\n" )
#
#               mv "$FISH_CONF_D_PATH"/"$file" \
#                  "$FISH_CONF_D_PATH"/10-"$file"
#         end
#
#
#    exit; end
