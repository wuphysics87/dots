#!/usr/bin/env fish

## Exit if not Interactive ##
status is-interactive || exit

## Short Hand ##
alias gcm 'git commit -m'
alias gs 'git status'
alias gbr 'git branch'
alias grm 'git rm -r'
alias gd 'git diff'
alias gl 'git log'

## git add all or passed list ##
function git_add

    set -l argc (count $argv)
    if test $argc -eq 0
        git add *
    else
        git add "$argv"
    end
end
alias ga git_add $argv


## git clone named repo from gitlab account ##
function git_clone
    for repo in $argv
        git clone "git@gitlab.com:wuphysics87/$repo.git"
    end
end
alias gcl git_clone


## git pull origin branch ##
function git_pull
    set branch (git rev-parse --abbrev-ref HEAD)
    git pull origin "$branch"
end
alias gpl git_pull


## git push branch ##
function git_push
    set branch (git rev-parse --abbrev-ref HEAD)
    git push origin "$branch"
end
alias gph git_push


## git checkout item ##
function git_checkout
    git checkout "$argv"
end
alias gco git_checkout
