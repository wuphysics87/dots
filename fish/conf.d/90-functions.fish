#!/usr/bin/env fish

function __my_test
    set -l cli_string  'echo \'hello\' "world"'
    commandline "$cli_string"
end

function __get_stats

    set -l cursor_position (commandline -C)
    set -l word_position (commandline -C -t)
    set -l full_line (commandline -b)
    echo "Cursor Position: $cursor_position" > "$HOME/test.txt"
    echo "Word Position:   $word_position" >> "$HOME/test.txt"
    echo "Full Line:       $full_line" >> "$HOME/test.txt"
    string match 'h' $full_line >> "$HOME/test.txt"

end

#function __insert_identical_pair
#
#    set match_array (string match -rna $argv[1] (commandline -b) | string split ' ' -f1)
#
#    if test (math ( count $match_array ) % 2) = 0
#        commandline -i (string repeat -n 2 $argv[1])
#        commandline -f forward-single-char
#        commandline -f backward-char
#    else
#        #echo "NO"
#        commandline -i $argv[1]
#    end
#
#end
#
#bind --user -M insert '"' '__insert_identical_pair \"'
#bind --user -M insert "'" "__insert_identical_pair \'"
#bind --user -M insert "`" "__insert_identical_pair \`"

#bind --user -M insert \cD __my_test
