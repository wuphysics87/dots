#!/usr/bin/env fish

# Set User and pyenv Paths ##
#   if test -n "$USER_PATHS_SET"
set -U USER_PATHS_SET true
set -Ux PYENV_ROOT "$HOME/.pyenv"
set PATH_LIST "$HOME/.cargo/bin" "$HOME/.local/bin" \
    "$HOME/.go/bin" "$HOME/.perl5/bin" \
    "$HOME/.config/emacs/bin" \
	$(fd . -t d $HOME/.config/bin| tr '\n' ' ') \
	"$HOME/.nerd-dictation" \
    "$HOME/.config/bin/" \
    "$HOME/.config/sway/bin/" \
    "$HOME/.config/hypr/bin"
set -U fish_user_paths $PATH_LIST \
    $PYENV_ROOT/bin \
    $fish_user_paths
#   end

## PERL PATH ##
if test -n "$PERL_ROOT"
    set -U PLENV_ROOT "$HOME/.plenv"
    set -U PERL_ROOT "$HOME/.perl5"
    set -U PATH "$PLENV_ROOT/shims" \
        "$PATH" "$PLENV_ROOT/bin"
    set -U PERL_MB_OPT "--install_base $PERL_ROOT"
    set -U PERL_MM_OPT "INSTALL_BASE=$PERL_ROOT"
    set -U PERL5LIB "$PERL_ROOT/lib/perl5"
    set -U PERL_LOCAL_LIB_ROOT "$PERL_ROOT"
end

## Set Remaining Interactive Variables
status is-interactive || exit
set QT_STYLE_OVERRIDE adwaita-dark
set DOOMDIR "$HOME/.config/doom/"
