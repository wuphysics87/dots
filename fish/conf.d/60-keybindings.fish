#!/usr/bin/env fish

## Continue if non-interactive Shell ##
status is-interactive || exit


## Set Vim Bindings anc Ctrl+Backspace ##
bind --user -M insert \b backward-kill-word

set -U fish_key_bindings fish_vi_key_bindings

############## define function for backspace
