#!/usr/bin/env fish

## Exit if not Interactive ##
status is-interactive || exit

alias ai 'ollama run deepseek-v2'
alias yt-dlp 'python /home/sean/.config/bin/yt-dlp'
alias y yazi
alias er 'sudo evremap remap /home/sean/.config/keyboard/evremap.config &'
## Colorize grep and diff ##
alias cc 'cd ~/.config && clear'
alias grep 'grep --color=auto'
alias diff 'diff --color=auto'
alias montog 'bash ~/.config/sway/bin/sway-monitor-toggle.bash'
alias monpos 'bash ~/.config/sway/bin/sway-change-monitor-position.bash'
alias ba 'batman'

alias glc 'glab repo clone (glab repo list -G --member -P 1000 | rg -o \'^[-/\w]*\' | sk)'

function __quick_zat
    set full_path (cat ~/.qzat | sk)
    set path (echo $full_path | sed -E 's/\/(\w|-)+$//')
    set file (echo $full_path | rev | cut -d '/' -f1 | rev)
    cd $path
    zathura "$file".pdf & run "$file".tex & emacsclient "$file".tex &
end

alias ds __quick_zat

## Get ip addr and Export With Symbol for Starship ##
set IP_ADDRESS (ip addr | \
                     sed -En "/inet.*enp24s0/s/ *inet ([0-9.]+)\/.*/\1/p")
export ICON_IP_ADDRESS="ﲾ $IP_ADDRESS"

## Colorize Bat ##
if test -e (command -v bat)
    alias bat 'bat --wrap=character -p   \
                            --force-colorization \
                            --theme Nord'
end


## Alias n to fasd directory
#alias n='fasd -sif'


# Interactive History Command
function __hist
    set -l argc (count $argv)
    if test $argc -eq 0
        history | sk | fish --no-config
    else
        test $argc -eq 1
        history | rg $argv | sk | fish --no-config
    end

end

alias hist __hist $argv

## Set default ls to exa ##
if command -v exa &>/dev/null
    export EXA_COLORS 'ln=36'
    alias ls '/usr/bin/exa --color=always -F --icons \
     	  	     			      --group-directories-first '
    alias lt 'ls -T -L3'
    alias la 'ls -a '
    alias ll 'ls -l'
    alias ll 'ls -la'
    alias ld 'lt -D'
end

## Alias Bluetooth ##
if command -v bluetoothctl &>/dev/null
    alias bt bluetoothctl
    alias shkz 'bt connect 20:74:CF:A6:B4:34 && \
                       pactl set-default-sink bluez_output.20_74_CF_A6_B4_34.1'
    alias sbar 'bt connect 40:16:3B:BD:B3:7D && \
                       pactl set-default-sink bluez_output.40_16_3B_BD_B3_7D.1'
end

## Alias shutdown, bios, and reboot ##
alias bios 'sudo systemctl reboot --firmware'
alias sdwn 'shutdown -P 0'
alias rb reboot


## ls after cd and clear ##
function __cd-ls --on-variable PWD
    pwd >/tmp/fish-cwd
    ls
end
alias clear '/usr/bin/clear;   ls'

## Alias rm and cp ##
alias rm='/usr/bin/rm -rf'
alias cp='/usr/bin/cp -r'
#alias cd='__cd-ls'

## Colorize man ##
set -x LESS_TERMCAP_us (printf "\e[04;38;5;146m")
set -x LESS_TERMCAP_md (printf "\e[01;38;5;74m")
set -x LESS_TERMCAP_so (printf "\033[01;44;33m")
set -x LESS_TERMCAP_mb (printf "\033[01;31m")
set -x LESS_TERMCAP_me (printf "\033[0m")
set -x LESS_TERMCAP_se (printf "\033[0m")
set -x LESS_TERMCAP_ue (printf "\e[0m")
set -l name (basename (status -f) .fish){_uninstall}

function $name --on-event $name
    set -e LESS_TERMCAP_md
    set -e LESS_TERMCAP_mb
    set -e LESS_TERMCAP_me
    set -e LESS_TERMCAP_ue
    set -e LESS_TERMCAP_us
    set -e LESS_TERMCAP_se
    set -e LESS_TERMCAP_so
end

function __hist
    set my_cmd (history | sk --height=75% --layout=reverse)

    #eval $my_cmd
    echo - cmd: "$my_cmd" >>$HOME/.local/share/fish/fish_history
    echo '  when:' $(date +%s) >>$HOME/.local/share/fish/fish_history
    test $(pwd) != $HOME && echo '  paths:' >>$HOME/.local/share/fish/fish_history && echo '    -' $HOME >>$HOME/.local/share/fish/fish_history
    eval $my_cmd && history merge

end

alias h __hist

eval (batpipe)
batman --export-env | source
alias watch 'batwatch'
alias f '$HOME/.config/bin/n'

