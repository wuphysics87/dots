import numpy as np
import os
import math
from tabulate import tabulate

trig_table = [["Theta", "sin", "cos", "tan"]]

def sin(angle):
    return math.trunc(1000*np.sin(np.radians(angle)))/1000

def cos(angle):
    return math.trunc(1000*np.cos(np.radians(angle)))/1000

def tan(angle):
    return math.trunc(1000*np.tan(np.radians(angle)))/1000

for x in range(0, 50, 5):
    trig_table += [[x, sin(x), cos(x), tan(x)]]

print(tabulate(trig_table))
