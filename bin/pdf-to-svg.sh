#!/usr/bin/env bash


NUM=`pdfinfo $1 | rg Pages: | rg -o "[0-9]+"`

for (( i=1; i<=$NUM; ++i )); do
    pdf2svg $1 p$i.svg $i
done
