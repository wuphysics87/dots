#!/usr/bin/env bash


NUM=`pdfinfo $1 | rg Pages: | rg -o "[0-9]+"`

for (( i=1; i<=$NUM; ++i )); do
    pdftk $1 cat $i output p$i.pdf
done
