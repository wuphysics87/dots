#!/bin/bash


INPUT="-1d-1w"
INPUT="11/28"

function date_from_days() {
    INPUT="+1d+1w"
    DATE_STRING=$(echo $INPUT | sed -En 's/y/year/; s/w/week/; s/d/day/p')

    FINAL_DATE=$(date "+%a %b(%m) %d" --date="$DATE_STRING")

    cal; printf "%s\n\n" "--------------------"
    cal "$DATE_STRING"

    printf "\n====================
            \n In $DATE_STRING,\
            \n    it will be \
            \n $FINAL_DATE \
            \n\n===================="
}

function days_from_date() {
    DOY_NOW=$(date +%j )
    DOY_FUTURE=$(date +%j --date="$INPUT")

    cal; printf "%s\n\n" "--------------------"
    FUTURE_DATE="$(echo $INPUT | tr '/' ' ') $(date +%Y)"
    DOM=$(echo $FUTURE_DATE | cut -d ' ' -f2)
    MON=$(echo $FUTURE_DATE | cut -d ' ' -f1)
    FUTURE_DATE="$DOM $MON $(date +%Y)"
    cal $FUTURE_DATE


    DATE_DELTA=$(echo $((DOY_FUTURE - DOY_NOW)) | tr '/' ' ')
    END_DATE+=$(date +%Y)

    printf "\n====================
            \n There are $DATE_DELTA days \
            \n     until \
                \n   $INPUT/$(date +%Y)
            \n===================="
}

main() {

    printf "\n====================\n\n"
    case "$INPUT" in
        *-* | *-*)
            date_from_days
            ;;
        */*)
            days_from_date
            ;;
        *) echo FAILED!;;
    esac

}

main
