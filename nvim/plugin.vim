" Set install plug.vim if doesn't exist
if ! filereadable(expand('~/.config/nvim/autoload/plug.vim'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ~/.config/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ~/.config/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

call plug#begin('~/.config/nvim/plugged')

	" ion syntax highlighting
	Plug 'vmchale/ion-vim' 			" close buffer without deleting window

	" Buffer Navigation
	Plug 'rbgrouleff/bclose.vim' 			" close buffer without deleting window
	Plug 'LeonB/vim-previous-buffer' 	    " adds sensible :previousBuffer

	" francoiscabrol/ranger.vim
	Plug 'francoiscabrol/ranger.vim' 		" Run ranger from within vim

	" vim-airline
	Plug 'vim-airline/vim-airline'
	Plug 'vim-airline/vim-airline-themes'

	" tmux move pane
	Plug 'christoomey/vim-tmux-navigator'

	"" Bash
	"Plug 'vim-scripts/bash-support.vim'

	" CSS
	Plug 'axvr/org.vim'	 	    " org-mode syntax highlighting

	" CSS
	Plug 'chrisbra/Colorizer'	" show hex colors in vim

	" tpope: The True UNIX Chad
	Plug 'tpope/vim-tbone' 				" Tmux support
	Plug 'tpope/vim-unimpaired'			" Extra bracket motions
	Plug 'tpope/vim-fugitive'			" Ex commands for git
	Plug 'tpope/vim-surround'			" change surrounding parenthesis
	Plug 'tpope/vim-repeat'				" Improves '.' for repeating motions from t-pope plugins
	" Plug 'jiangmiao/auto-pairs'				" Create pairs of brackets and quotes

	" Shell Scripting
	Plug 'itspriddle/vim-shellcheck'	" real time shell scripting hints/suggestions

	" Linting and completion
	" Plug 'dense-analysis/ale' 			" Check syntax in Vim asynchronously
    "lug 'ycm-core/YouCompleteMe'		" run `pip3 install --user neovim`

	" Themes

	"Plug 'altercation/vim-colors-solarized'
	"Plug 'https://gitlab.com/iscreaman23/vim-tomorrow-night-theme.git'
    " Plug 'arcticicestudio/nord-vim'
    Plug 'squarefrog/tomorrow-night.vim'
call plug#end()
