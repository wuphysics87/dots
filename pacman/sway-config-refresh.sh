#!/usr/bin/env sh

HOSTNAME=$(hostnamectl | sed -En 's/ Static hostname: (.*)/\1/p')

sudo sed -E "s/XXXXX/$HOSTNAME/g" sway.desktop | sudo tee  /usr/share/wayland-sessions/sway.desktop
