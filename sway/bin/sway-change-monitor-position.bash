#!/usr/bin/env bash

read -a MONITORS < <(swaymsg -r -t get_outputs | jq '.[] | select(.type == "output") | (.name, .rect.x, .rect.y)' | tr '\n' ' ')

MONITOR_ONE=($(echo "${MONITORS[*]:6}" | tr '"' ' '))
MONITOR_TWO=($(echo "${MONITORS[*]:3:3}" | tr '"' ' '))
MONITOR_THREE=($(echo "${MONITORS[*]:0:3}" | tr '"' ' '))

if [[ "${MONITOR_TWO[1]}" == "2000" ]]; then
    swaymsg output DP-1 pos 1920 0 res 1920 1080
    swaymsg output HDMI-A-1 pos 0 0 res 1920 1080
    swaymsg output DP-2 pos 3840 0 res 1920 1080

else
    swaymsg output DP-1 pos 2000 2000 res 1920 1080
    swaymsg output HDMI-A-1 pos 0 0 res 1920 1080
    swaymsg output DP-2 pos 4000 0 res 1920 1080

fi
