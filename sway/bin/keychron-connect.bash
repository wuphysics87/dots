#/usr/bin/env bash

until bluetoothctl connect "$(bluetoothctl devices | rg '.* ([dw:]+) Keychron.*' -r '$1' )"; do sleep 1; done
