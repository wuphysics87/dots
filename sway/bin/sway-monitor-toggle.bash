#!/usr/bin/env bash

MONITOR_STATUS="$(swaymsg -rt get_outputs | jq '.[].power' | tr '\n' ' ')"
[[ "$MONITOR_STATUS" == *"false"* ]] && POWER_STATE="on" || POWER_STATE="off"
swaymsg output DP-2 power "$POWER_STATE"
swaymsg output HDMI-A-1 power "$POWER_STATE"

swaymsg -r -t get_tree | jq 'recurse(.nodes[]) | select( .name=='"$WORKSPACE_NAME"') | recurse(.nodes[]) | select(.focused==true) | (.rect.x, .rect.y, .rect.width, .rect.height)' | tr '\n' ' '
