#!/usr/bin/env bash

NEW_VOLUME=$1

#NEW_VOLUME=-5


DEFAULT_SINK_NAME=$(pactl get-default-sink)

if [[ -z $DEFAULT_SINK_NAME  ]]; then
    DEFAULT_SINK_NUM="$(pactl list short sinks | grep RUNNING | cut -f1)"
else
    DEFAULT_SINK_NUM="$(pactl list short sinks | grep $DEFAULT_SINK_NAME | cut -f1)"
fi

CURRENT_VOLUME=$(pactl get-sink-volume "$DEFAULT_SINK_NAME" | sed -En 's/[[:space:]]+/ /g; s/.*dB,//p' | cut -d '/' -f 2)
CURRENT_VOLUME="${CURRENT_VOLUME:0:-2}"

echo "$CURRENT_VOLUME"
if [ "$CURRENT_VOLUME" -ge 100 ] && [ "$NEW_VOLUME" == 10 ]; then
   NEW_VOLUME=100
else
   NEW_VOLUME=$(( CURRENT_VOLUME + NEW_VOLUME ))
fi

pactl set-sink-volume "$DEFAULT_SINK_NAME" "$NEW_VOLUME"%
