#!/usr/bin/env bash

swaymsg -t get_outputs | rg "transform.*90" && swaymsg output DP-2 transform 90 anticlockwise || swaymsg output DP-2 transform 90 clockwise
