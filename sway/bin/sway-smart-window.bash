#!/usr/bin/env bash

ACTION=$1
DIRECTION=$2
MODIFIER=$3

get_window_data() {
    swaymsg -r -t get_tree | jq 'recurse(.nodes[]) | select( .name=='"$WORKSPACE_NAME"') | recurse(.nodes[]) | select(.focused==true) | (.rect.x, .rect.y, .rect.width, .rect.height)' | tr '\n' ' '
}

get_workspace_data() {
    swaymsg -rt get_workspaces | jq '.[] | select(.focused==true) | (.name, .rect.x, .rect.y, .rect.width, .rect.height)' | tr '\n' ' '
}

focus_vertically() {

    read -r WORKSPACE_NAME WORKSPACE_X WORKSPACE_Y WORKSPACE_WIDTH WORKSPACE_HEIGHT <<<"$(get_workspace_data)"
    read -r WINDOW_X WINDOW_Y WINDOW_WIDTH WINDOW_HEIGHT <<<"$(get_window_data)"

    DIRECTION_INDEX=$([ "$DIRECTION" == "up" ] && echo 1 || echo -1)

    MONITOR=${WORKSPACE_NAME:1:1}
    WORKSPACE_INDEX=${WORKSPACE_NAME:2:-1}
    LAST_WORKSPACE=$([ "$MONITOR" == "C" ] && echo 5 || echo 2)
    NEW_WORKSPACE=$((DIRECTION_INDEX + WORKSPACE_INDEX))

    if [[ "$NEW_WORKSPACE" -lt 1 ]]; then
        NEW_WORKSPACE=$LAST_WORKSPACE
    elif [[ "$NEW_WORKSPACE" -gt "$LAST_WORKSPACE" ]]; then
        NEW_WORKSPACE=1
    fi

    if [[ "$ACTION" == "move" ]]; then
        MOVEMENT_DIRECTIVE="move window to workspace ${MONITOR}${NEW_WORKSPACE}"
    fi
    FOCUS_DIRECTIVE="workspace ${MONITOR}${NEW_WORKSPACE}"

    if [[ $((WINDOW_HEIGHT + WINDOW_Y)) -ge $((WORKSPACE_HEIGHT + WORKSPACE_Y)) ]] && [[ "$DIRECTION" == "down" ]]; then
        [[ $ACTION == "move" ]] && swaymsg "$MOVEMENT_DIRECTIVE"
        swaymsg "$FOCUS_DIRECTIVE"
    elif [[ $WINDOW_Y -eq $WORKSPACE_Y ]] && [[ "$DIRECTION" == "up" ]]; then
        #swaymsg workspace "${MONITOR}${NEW_WORKSPACE}"
        [[ $ACTION == "move" ]] && swaymsg "$MOVEMENT_DIRECTIVE"
        swaymsg "$FOCUS_DIRECTIVE"
        #swaymsg "$MOVEMENT_DIRECTIVE"
    elif [[ -x $WINDOW_X ]]; then
        [[ $ACTION == "move" ]] && swaymsg "$MOVEMENT_DIRECTIVE"
        #swaymsg "$MOVEMENT_DIRECTIVE"
        swaymsg "$FOCUS_DIRECTIVE"
        #swaymsg workspace "${MONITOR}${NEW_WORKSPACE}"
    fi

    if [[ $ACTION == "focus" ]]; then
        swaymsg focus "$DIRECTION"
    else
        swaymsg move "$DIRECTION"
    fi
    # swaymsg workspace "${MONITOR}${NEW_WORKSPACE}"
}

main() {

    if [[ $ACTION == "monitor" ]]; then
        swaymsg move window to output "$DIRECTION"
        swaymsg focus output "$DIRECTION"
    elif [[ $ACTION == "workspace" ]]; then
        swaymsg move window to workspace "$DIRECTION"
        swaymsg workspace "$DIRECTION"
        exit
    elif [[ $DIRECTION == "left" ]] || [[ $DIRECTION == "right" ]]; then
        swaymsg "$ACTION" "$DIRECTION"
        exit
    else
        focus_vertically
    fi

}

main
