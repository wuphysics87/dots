#!/usr/bin/env bash

get_dims(){
	swaymsg -t get_tree | jq 'recurse(.nodes[]) | select(.focused==true) | recurse(.nodes[]) | (.rect.height, .rect.width)' | tr '\n' ' '
}

read -r HEIGHT WIDTH <<<"$(get_dims)"

if [[ "$HEIGHT" -gt "$WIDTH" ]]; then
	swaymsg splitv; alacritty
else
	swaymsg splith; alacritty
fi
