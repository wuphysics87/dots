#!/usr/bin/env bash

MONITOR_STATUS="$(swaymsg -rt get_outputs | grep 2000)"

[[ "$MONITOR_STATUS" == *"2000"* ]] && POS="1920 0" || POS="2000 2000"

swaymsg output DP-1 pos "$POS" res 1920 1080
