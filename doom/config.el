;; (package! emacsql :pin "491105a01f58bf0b346cbc0254766c6800b229a2")
;; (use-package emacsql
;;   :ensure t
;;   :pin ((emacsql . "491105a01f58bf0b346cbc0254766c6800b229a2")))
(use-package! org-roam
  :after org
  :config
    (advice-remove 'org-roam-db-query '+org-roam-try-init-db-a))

(use-package! emacs
  :init ;;(setq! wuphys/doom-dashboard-sound t)
        (setq! fancy-splash-image "~/.config/doom/dashboard/doom-logo.png"
               +doom-dashboard-banner-padding '(0 . 0)
               display-buffer-alist '(("\\*Messages\\*"
                                       display-buffer-no-window)))
          ;; (add-hook! +doom-dashboard-mode :append
          ;;   (when (wuphys/doom-dashboard-sound)
          ;;         (async-shell-command "mpv ~/.config/doom/dashboard/E1M1-cut.mp3"
          ;;                              "\*Messages\*" nil)))
  :config (set-frame-font "Fantasque Sans Mono 8" nil t)
          (fset 'yes-or-no-p 'y-or-n-p)
          (setq! org-directory "~/notes/")
          (setq! display-line-numbers-type nil
                 auto-revert-verbose t
                 global-auto-revert-mode t
                 auto-revert-use-notify nil
                 doom-theme 'doom-nord-aurora
                 vc-follow-symlinks t
                 writeroom-fullscreen-effect t
                 global-whitespace-mode t
                 whitespace-mode t))

(use-package! doom-modeline
  :config (setq! doom-modeline-minor-modes nil
                 doom-modeline-buffer-encoding nil
                 doom-modeline-icon (display-graphic-p)
                 doom-modeline-major-mode-icon t))

(use-package! org
  :config (setq org-startup-folded t
                org-ellipsis " 󰞖"
                org-directory "~/notes/"))

(setq! org-roam-directory "~/notes/"
       org-roam-completion-everywhere t)

(setq org-todo-keywords nil)

(defun wuphys/org-insert-checkbox ()
    (interactive)
    (evil-beginning-of-line)
    (insert "- [ ]"))

(defun wuphys/compile-buffer ()
  (interactive)
  (add-hook 'after-save-hook 'org-latex-export-to-pdf))

(setq org-todo-keywords
      '((sequence  "TODO(t)" "NEXT(n)"  "WAIT(w)" "|" "DONE(d)")
             (type "IDEA(i)" "LKUP(l)" "|")
             (sequence "[ ](o)" "[X](x)")))

(setq org-todo-keyword-faces
      '(("TODO" . org-agenda-date)
        ("NEXT" . +org-todo-active)
        ("WAIT" . all-the-icons-orange)
        ("DONE" . org-done)
        ("IDEA" . +org-todo-onhold)
        ("LKUP" . all-the-icons-lblue)))

(org-mode-restart)

(setq! +org-capture-changelog-file   nil
       +org-capture-projects-file    nil
       +org-capture-journal-file     nil
       +org-capture-notes-file       nil)

(setq! org-capture-templates
        `(("t" "TODO(t)" entry
           (file+headline +org-capture-todo-file "Inbox")
           ,(concat "* TODO %? \n%a") :prepend t)
          ("n" "NEXT(n)" entry
           (file+headline +org-capture-todo-file "Now")
           ,(concat "* NEXT %? \n%a") :prepend t)
          ("i" "IDEA(i)" entry
           (file+headline +org-capture-todo-file "Inbox")
           ,(concat "* IDEA %? \n%a" "") :prepend t)
          ("l" "LKUP(l)" entry
           (file+headline +org-capture-todo-file "Inbox")
           ,(concat "* LKUP %? \n%a" "") :prepend t)
          ("d" "DUMP(d)" plain
           (file "~/notes/dump.org" ) "* %a\n%c-----" :immediate-finish t)))

(map! "C-;"         #'execute-extended-command)

(setq +evil-want-o/O-to-continue-comments nil)

(defun evil-command-window-ex ()
            (interactive)
            (evil-ex))

(use-package! evil
  :config (setq! evil-echo-state nil
                 evil-split-window-below t
                 evil-vsplit-window-right t
                 evil-search-wrap nil
                 evil-mode-line-format nil))

(defun wuphys/cycle-and-insert-item-below ()
  (interactive)
  (org-cycle)
  (+org/insert-item-below 1))

(after! (evil-org org evil)
 (map! :map (evil-motion-state-map evil-org-mode-map org-mode-map)
       "C-f"   #'evil-ex-search-forward
       "C-RET" #'wuphys/cycle-and-insert-item-below
       "C-;"         #'execute-extended-command))
(add-hook! +doom-dashboard-mode-hook
 (map! :map evil-motion-state-map
       "C-e"  #'evil-end-of-line))

(map! "C-c c"        #'org-capture
      "C-S-p"        #'yank-from-kill-ring
      "C-c C-;"      #'iedit-mode
      "C-c t"        #'org-todo
      "C-a"          #'evil-first-non-blank
      "C-q"          #'kill-this-buffer
      "C-;"          #'execute-extended-command
      "C-h RET"      #'man
      "C-f"          #'dired
      "C-h b s"        #'my/which-key-sort-type
      "C-h i"        #'info-other-window
      "M-<return>"   #'+vterm/toggle
      "H-s-<return>"   #'+eshell/toggle
      "M-s-<return>"   #'+eshell/toggle
      "C-:"          #'ivy-resume
      "C-/"          #'swiper
      "C-s"          #'save-buffer
      :leader

      ;; (:prefix ("e" . "Ellama")
      ;;  :desc "Ask About"       "a"  #'ellama-ask-about
      ;;  (:prefix ("A" . "Ask")
      ;;   :desc "Selection"    "s"  #'ellama-ask-selection
      ;;   :desc "Line"         "l"  #'ellama-ask-line)

      ;;  :desc "Add buffer"       "b"  #'ellama-add-buffer
      ;;  :desc "Change"       "c"  #'ellama-change
      ;;  :desc "Complete"       "C"  #'ellama-complete

      ;;  :desc "Define"       "d"  #'ellama-define
      ;;  :desc "Improve Wording"    "i"  #'ellama-improve-wording

      ;;  (:prefix ("I" . "Improve")
      ;;   :desc "Conciseness"     "c"  #'ellama-improve-conciseness
      ;;   :desc "Grammar"         "g"  #'ellama-improve-grammar)

      ;;  (:prefix ("m" . "Make")
      ;;   :desc "list"     "c"  #'ellama-make-list
      ;;   :desc "format"         "g"  #'ellama-make-format
      ;;   :desc "table"         "g"  #'ellama-make-table)

      ;;  (:prefix ("n" . "Session")
      ;;   :desc "send last message"     "m"  #'ellama-chat-send-last-message
      ;;   :desc "remove"         "x"  #'ellama-session-remove
      ;;   :desc "rename"         "r"  #'ellama-session-rename
      ;;   :desc "switch"         "s"  #'ellama-session-switch)

      ;;  :desc "code add"    "o"  #'ellama-code-add

      ;;  (:prefix ("O" . "Code")
      ;;   :desc "complete"    "c"  #'ellama-code-complete
      ;;   :desc "improve "    "i"  #'ellama-code-improve
      ;;   :desc "review"      "e"  #'ellama-code-review
      ;;   :desc "edit"        "r"  #'ellama-improve-edit)

      ;;  :desc "Generate Commit Message"    "m"  #'ellama-generate-commit-message

      ;;  :desc "provider select"    "p"  #'ellama-provider-select

      ;;  :desc "Solve Reasoning"    "m"  #'ellama-solve-reasoning-problem
      ;;  :desc "Solve Domain"    "m"  #'ellama-solve-somain-specific-problem

      ;;  :desc "summarize"    "s"  #'ellama-summarize
      ;;  :desc "context add selection"    "S"  #'ellama-context-add-selection
      ;;  :desc "chat"    "t"  #'ellama-chat

      ;;  (:prefix ("t" . "translate")
      ;;   :desc "translate"         "t"  #'ellama-translate
      ;;   :desc "translate buffer"      "b"  #'ellama-translate-buffer
      ;;   :desc "translation enable"    "e"  #'ellamab-translate-enable
      ;;   :desc "summarize killring"    "k"  #'ellama-summarize-killring
      ;;   :desc "Summarize Webpage"    "w"  #'ellama-summarize-webpage
      ;;   :desc "translation disable"    "d"  #'ellama-chat-translation-disabled))
      (:prefix ("t" . "toggle")
        :desc "Center Cursor" "c" 'centered-cursor-mode
        :desc "Company Mode"  "C" 'company-mode
        :desc "Visual Column Mode"  "v" 'visual-fill-column-mode
        :desc "Visual Column Indicator"  "V" 'display-fill-column-indicator-mode
        :desc "Rainbow Delimiters"  "R" 'rainbow-delimiters-mode
        :desc "Whitespace Mode"  "W" 'whitespace-mode)
      (:prefix ("i" . "insert")
        "i" 'all-the-icons-insert)
      (:prefix ("g" . "git")
        "n"  'git-gutter:next-hunk
        "p"  'git-gutter:prev-hunk)
      (:prefix ("f" . "file")
        "c" 'wuphys/find-file-config-org
        "m" 'doom/move-this-file
        "t" 'wuphys/find-file-todo)
      (:prefix ("w" . "window")
        "a"  'ace-window)
      (:prefix ("s" . "search")
        "R"  'counsel-mark-ring
        "h"  'counsel-imenu
        "r"  'rg)
      (:prefix ("m" . "localleader")
        "x" 'wuphys/org-insert-checkbox
        "X" 'org-toggle-checkbox))


(map! :v "s" 'evil-visualstar/begin-search-forward)

(map! :leader
      (:prefix-map ("o" . "open")
        "n" #'org-roam-buffer-toggle
        "N" #'org-roam-buffer-display-dedicated)
      (:prefix-map ("n" . "note")
        "D" #'org-roam-demote-entire-buffer
        "n" #'org-roam-capture
       :desc "Roam ripgrep" "s" #'wuphys/org-roam-rg-search
       :desc "Sync db"      "S" #'org-roam-db-sync
        "f" #'org-roam-node-find
        "F" #'org-roam-ref-find
        "g" #'org-roam-graph
        "i" #'org-roam-node-insert
        "I" #'org-id-get-create
        "n" #'org-roam-capture
      :desc "Roam Refile" "r" #'org-roam-refile
     (:prefix ("a" . "annotate")
            "a" #'org-roam-alias-add
            "A" #'org-roam-alias-remove
            "t" #'org-roam-tag-add
            "T" #'org-roam-tag-remove
            "r" #'org-roam-ref-add
            "R" #'org-roam-ref-remove)))

(setq-default evil-cross-lines t)

(defun evil-next-line--check-visual-line-mode (orig-fun &rest args)
  (if visual-line-mode
      (apply 'evil-next-visual-line args)
    (apply orig-fun args)))

(advice-add 'evil-next-line :around
            'evil-next-line--check-visual-line-mode)

(defun evil-previous-line--check-visual-line-mode (orig-fun &rest args)
  (if visual-line-mode
      (apply 'evil-previous-visual-line args)
    (apply orig-fun args)))

(advice-add 'evil-previous-line :around
            'evil-previous-line--check-visual-line-mode)

;; (add-to-list 'load-path  (expand-file-name "~/.config/emacs/.local/straight/repos/ion-mode"))
(require 'ion-mode)
(autoload 'ion-mode (locate-library "ion-mode") "Ion major mode" t)
(add-to-list 'auto-mode-alist '("\\.ion\\'" . ion-mode))
(add-to-list 'auto-mode-alist '("/ion/initrc" . ion-mode))

(use-package! magit
  :config (map! :map magit-mode-map
            :m "<tab>" #'magit-section-toggle
            :m "TAB"   #'magit-section-toggle
            :n "<tab>" #'magit-section-toggle
            :n "TAB"   #'magit-section-toggle)
          (set-popup-rule! "^magit:"
                             :side     'right
                             :size     .35
                             :width    .35
                             :modeline nil
                             :quit     t))

(use-package! all-the-icons-ibuffer
  :config
    (setq ibuffer-formats
            `((mark " " (icon 2 2 :left :elide)
                      ,(propertize "" 'display `(space :align-to 8))
                      (name 30 50 :left :elide)
                      " " filename-and-process+)
                (mark " " (name 20 -1) " " filename))))

;; (set-popup-rule! "^\\*helpful"
;;                       :side     'right
;;                       :width    .50
;;                       :modeline nil
;;                       :quit     t)

;; (set-popup-rule! "^\\*\\(helpful\\|info\\|Man\\|Help\\)"
;;                       :side     'right
;;                       :size     .3
;;                       :width    .3
;;                       :modeline nil
;;                       :quit     t)

;; (setq Man-notify-method 'aggressive)
;;(add-hook! (helpful-mode Man-mode Info-mode) centered-cursor-mode)

(setq default-alias-fallback "everything")

;; (setq neo-hidden-regexp-list '("^\\." "^\\.\\(?:git\\|hg\\|svn\\)$" "\\.\\(?:pyc\\|o\\|elc\\|lock\\|css.map\\|class\\)$" "^\\(?:node_modules\\|vendor\\|.\\(project\\|cask\\|yardoc\\|sass-cache\\)\\)$" "^\\.\\(?:sync\\|export\\|attach\\)$" "~$" "^#.*#$"))


;;  (add-hook 'neotree-mode-hook
;;        (lambda ()

;;          (define-key evil-normal-state-local-map (kbd "v") 'neotree-enter-vertical-split)
;;          (define-key evil-normal-state-local-map (kbd "s") 'neotree-enter-horizontal-split)
;;          (define-key evil-normal-state-local-map (kbd "g j") 'neotree-select-next-sibling-node)
;;          (define-key evil-normal-state-local-map (kbd "C-a") 'neotree-collapse-all)
;;          (define-key evil-normal-state-local-map (kbd "g k") 'neotree-select-previous-sibling-node)))

(with-eval-after-load 'doom-themes
  (doom-themes-neotree-config))

(setq treemacs-show-hidden-files nil)

(setq! ivy-use-selectable-prompt t
       ivy-use-virtual-buffers t
       enable-recursive-minibuffers t)

(setq define-it-show-dictionary-definition t)
(setq define-it-show-wiki-summary t)
(setq define-it-show-google-translate nil)
(setq define-it-output-choice 'view)

(use-package company
  :defer 2
  :diminish
  :custom
  (company-begin-commands '(self-insert-command))
  (company-idle-delay .1)
  (company-minimum-prefix-length 2)
  (company-show-numbers t)
  (company-tooltip-align-annotations 't)

  (company-dabbrev-downcase 0)
  (company-idle-delay 0)
 
  (global-company-mode t))
(defun tab-indent-or-complete ()
  (interactive)
  (if (minibufferp)
      (minibuffer-complete)
    (if (or (not yas-minor-mode)
            (null (do-yas-expand)))
        (if (check-expansion)
            (company-complete-common)
          (indent-for-tab-command)))))

(global-set-key [backtab] 'tab-indent-or-complete)

;; (setq! deft-extensions '("org")
;;        deft-recursive t
;;        deft-directory "~/notes/")

(winner-mode +1)
(define-key winner-mode-map (kbd "<C-down>") #'winner-undo)
(define-key winner-mode-map (kbd "<C-up>") #'winner-redo)

(after! (org latex)
        (add-to-list 'latex-standard-block-names '"gather*")
        (add-to-list 'latex-standard-block-names '"cases")
        (add-to-list 'latex-standard-block-names '"align*"))

;; (setq org-src-fontify-natively t)
;; (add-hook 'org-mode-hook 'langtool-ignore-fonts-minor-mode)

;; (langtool-ignore-fonts-add 'org-mode-hook  '(font-lock-comment-face
;; 					  font-latex-math-face font-latex-string-face))
;; (langtool-ignore-fonts-add 'latex-mode-hook  '(font-lock-comment-face
;; 					  font-latex-math-face font-latex-string-face))

;; (setq vterm-shell "/bin/fish")
;; (setq vterm-term-environment-variable "eterm-color")
;; (set-popup-rule! "^\\*doom:vterm"
;;                       :side     'bottom
;;                       :size     .35
;;                       :height   .35
;;                       :width    .40
;;                       :modeline nil
;;                       :quit     t)
;; (set-popup-rule! "^\\*doom:eshell"
;;                       :side     'bottom
;;                       :size     .35
;;                       :height   .35
;;                       :width    .40
;;                       :modeline nil
;;                       :quit     t)
;; (add-hook! vterm-mode
;; 		  (global-prettify-symbols-mode -1))

(remove-hook 'doom-first-buffer-hook #'smartparens-global-mode)

(with-eval-after-load 'evil
  (scroll-on-jump-advice-add evil-undo)
  (scroll-on-jump-advice-add evil-redo)
  (scroll-on-jump-advice-add evil-jump-item)
  (scroll-on-jump-advice-add evil-jump-forward)
  (scroll-on-jump-advice-add evil-jump-backward)
  (scroll-on-jump-advice-add evil-ex-search-next)
  (scroll-on-jump-advice-add evil-ex-search-previous)
  (scroll-on-jump-advice-add evil-forward-paragraph)
  (scroll-on-jump-advice-add evil-backward-paragraph)
  (scroll-on-jump-advice-add evil-goto-mark)

  ;; Actions that themselves scroll.
  (scroll-on-jump-with-scroll-advice-add evil-goto-line)
  (scroll-on-jump-with-scroll-advice-add evil-scroll-down)
  (scroll-on-jump-with-scroll-advice-add evil-scroll-up)
  (scroll-on-jump-with-scroll-advice-add evil-scroll-line-to-center)
  (scroll-on-jump-with-scroll-advice-add evil-scroll-line-to-top)
  (scroll-on-jump-with-scroll-advice-add evil-scroll-line-to-bottom))

(with-eval-after-load 'goto-chg
  (scroll-on-jump-advice-add goto-last-change)
  (scroll-on-jump-advice-add goto-last-change-reverse))

(global-set-key (kbd "<C-M-next>") (scroll-on-jump-interactive 'diff-hl-next-hunk))
(global-set-key (kbd "<C-M-prior>") (scroll-on-jump-interactive 'diff-hl-previous-hunk))

;; (prefer-coding-system 'utf-8)
;; (set-default-coding-systems 'utf-8)
;; (set-language-environment 'utf-8)
;; (set-selection-coding-system 'utf-8)
(set-language-environment "UTF-8")
